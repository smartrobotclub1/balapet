enum {
  // Моторы
  DC_LEFT_DIR = 4, // Правый
  DC_LEFT_POW = 5,
  DC_RIGHT_DIR = 6, // Левый
  DC_RIGHT_POW = 7,
  // Датчики
  RS = 8, // Правый
  MS = 9, // Средний
  LS = 10 // Левый
};
void setup(){
  Serial.begin(9600);
  pinMode(DC_LEFT_DIR, OUTPUT);
  pinMode(DC_LEFT_POW, OUTPUT);
  pinMode(DC_RIGHT_DIR, OUTPUT);
  pinMode(DC_RIGHT_POW, OUTPUT);
}
void loop(){
  
  ir(); // Тест трек-сенсоров
  bool left = !digitalRead(LS), middle = !digitalRead(MS), right = !digitalRead(RS);
  if (left == true && middle == false && right == true) { // 101
    Serial.println("Вперёд!");
    go();
  }
  
  else if (left == false && right == true) { // 001
    Serial.println("Влево!");
    leftTurn();
  }
  else if (left == true && right == false) { // 100
    Serial.println("Вправо!");
    rightTurn();
  }
}
void STOP(){
  digitalWrite(DC_LEFT_DIR, LOW);
  digitalWrite(DC_LEFT_POW, LOW);
  digitalWrite(DC_RIGHT_DIR, LOW);
  digitalWrite(DC_RIGHT_POW, LOW);
}
void go(){
  digitalWrite(DC_LEFT_DIR, LOW );
  digitalWrite(DC_LEFT_POW, HIGH);
  digitalWrite(DC_RIGHT_DIR, HIGH);
  digitalWrite(DC_RIGHT_POW, LOW);
}
void leftTurn(){
  digitalWrite(DC_LEFT_DIR, LOW);
  digitalWrite(DC_LEFT_POW, LOW);
  digitalWrite(DC_RIGHT_DIR, HIGH);
  digitalWrite(DC_RIGHT_POW, LOW);
}
void rightTurn(){
  digitalWrite(DC_LEFT_DIR, LOW);
  digitalWrite(DC_LEFT_POW, HIGH);
  digitalWrite(DC_RIGHT_DIR, LOW);
  digitalWrite(DC_RIGHT_POW, LOW);
}
void ir(){ // 1 - чёрный, 0 - белый
  Serial.print(digitalRead(RS));
  Serial.print(digitalRead(MS));
  Serial.println(digitalRead(LS));
}
