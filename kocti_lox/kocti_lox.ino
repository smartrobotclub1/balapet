enum {
  // Моторы
  IN1 = 4, // Правый
  IN2 = 5,
  IN3 = 6, // Левый
  IN4 = 7,
  // Датчики
  RS = 8, // Правый
  MS = 9, // Средний
  LS = 10 // Левый
};
void setup(){
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
}
void loop(){
  //motors(); // Тест моторов
  ir(); // Тест трек-сенсоров
  bool left = !digitalRead(LS), middle = !digitalRead(MS), right = !digitalRead(RS);
  if (left == true && middle == false && right == true) { // 010
    Serial.println("Вперёд!");
    go();
  }
  
  else if (left == true && right == false) { // 100
    Serial.println("Влево!");
    leftTurn();
  }
  else if (left == false && right == true) { // 001
    Serial.println("Вправо!");
    rightTurn();
  }
}
void STOP(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
}
void go(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
}
void leftTurn(){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, HIGH);
  digitalWrite(IN4, LOW);
}
void rightTurn(){
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, HIGH);
}
void ir(){ // 1 - чёрный, 0 - белый
  Serial.print(digitalRead(RS));
  Serial.print(digitalRead(MS));
  Serial.println(digitalRead(LS));
}
